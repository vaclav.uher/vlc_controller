from apscheduler.schedulers.background import BackgroundScheduler

from settings import settings, RC_HOST, RC_PORT, core_logger
from vlc_connector import VLCClient
from vlc_controller import VlcController
from actions import do_action


# run VLC streamer
if settings['run_vlc']:
    vlc_server = VlcController()
    core_logger.info('Starting VLC server.')
    vlc_server.start()

# init telnet client
core_logger.info('initializing VLC telnet client.')
vlc_rc = VLCClient(host=settings[RC_HOST], port=settings[RC_PORT])
# TODO handle connection refused when vlc is not running

sched = BackgroundScheduler()
# examples: https://apscheduler.readthedocs.io/en/latest/modules/triggers/cron.html#expression-types
sched.add_job(lambda: do_action(url='www'), 'cron',
              month='*',
              day_of_week='mon-fri',
              hour='*',
              minute='*/2',
              second='0')
core_logger.info('Adding scheduler job.')

core_logger.info('Starting scheduler.')
sched.start()
