import os
import unittest
from actions import transcode_video, audio_to_video
from settings import DATA_FOLDER


class TestTranscoder(unittest.TestCase):
    test_folder = os.path.join(DATA_FOLDER, 'test')
    example_video = os.path.join(test_folder, 'pres_vid.mp4')
    example_video_trans = os.path.join(test_folder, 'pres_vid_trans.mkv')
    example_video_trans2 = os.path.join(test_folder, 'pres_vid_trans2.mkv')

    #def test_include_logo(self):
    #    transcode_video(source=self.example_video, destination=self.example_video_trans, insert_logo=True)

    #def test_exclude_logo(self):
    #    transcode_video(source=self.example_video, destination=self.example_video_trans2, insert_logo=False)

    def test_audio_to_video(self):
        image = os.path.join(DATA_FOLDER, 'logo', 'test-pattern.png')
        audio = os.path.join(DATA_FOLDER, 'municipal_radio', '07_-_Florian_a_Kilian.mp3')
        destination = os.path.join(DATA_FOLDER, 'municipal_radio', 'track.mkv')
        audio_to_video(image=image, audio=audio, destination=destination)


if __name__ == '__main__':
    unittest.main()
