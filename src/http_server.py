from os import listdir
from os.path import isfile, isdir
from shutil import move, rmtree
import tempfile

from flask import Flask, render_template, request, redirect, jsonify
from flask_dropzone import Dropzone
from flask_uploads import patch_request_class
from werkzeug.utils import secure_filename

from actions import update_presentation, update_municipal_radio, transcode_video
from settings import *
from tasks import Tasks
from controllers import vlc_rc


app = Flask(__name__, template_folder=join(SETTINGS_FOLDER, 'templates'), static_folder=join(SETTINGS_FOLDER, 'static'))
dropzone = Dropzone(app)

# Dropzone settings
app.config['DROPZONE_UPLOAD_MULTIPLE'] = True
app.config['DROPZONE_ALLOWED_FILE_CUSTOM'] = True
app.config['DROPZONE_ALLOWED_FILE_TYPE'] = 'video/*'
app.config['DROPZONE_REDIRECT_VIEW'] = 'dashboard'
app.config['DROPZONE_UPLOAD_ACTION'] = 'dashboard'


# Uploads settings
app.config['UPLOADED_FILES_DEST'] = UNUSED_FOLDER
patch_request_class(app, size=1024*1024*1024*8)  # set maximum file size, default is 8 GiB


def str_to_bool(string):
    if string.lower() in ['yes', 'true', '1', 'y']:
        return True
    else:
        return False


@app.route('/', methods=['GET'])
@app.route('/dashboard', methods=['GET', 'POST'])
def dashboard():
    if request.method == 'POST':
        core_logger.info('Uploading video.')
        file_obj = request.files
        for file in file_obj:
            video_file = file_obj[file]
            # check if the post request has the file part
            # if user does not select file, browser also
            # submit an empty part without filename
            if video_file.filename == '':
                continue
            if video_file and allowed_file(video_file.filename):
                filename = secure_filename(video_file.filename)
                destination = join(UNUSED_FOLDER, filename)
                temp_upload = tempfile.NamedTemporaryFile()
                if exists(destination):
                    core_logger.warn('File {} already exists.')
                    continue
                core_logger.info('Saving uploaded file to {}.'.format(destination))
                video_file.save(temp_upload)
                transcode_video(temp_upload.name, destination, True)
                temp_upload.close()

    core_logger.info('Dashboard rendering.')
    tasks = Tasks([MUNICIPAL_RADIO, ADVERTISEMENT, CLIPS, UNUSED])
    try:
        playlist = vlc_rc.playlist()
    except ConnectionRefusedError as e:
        core_logger.debug(e)
        playlist = None
    return render_template('dashboard.html',
                           clips=tasks.clips(), unused=tasks.unused(), playlist=playlist,
                           presentation_id=settings[PRESENTATION_ID])


@app.route('/template')
def template():
    core_logger.info('Template rendering.')
    return render_template('template.html')


@app.route('/action/play_pause')
def vlc_play_pause():
    core_logger.info('Action/play_pause.')
    vlc_rc.play_pause()
    return redirect("/dashboard", code=302)


@app.route('/action/next')
def vlc_next():
    core_logger.info('Action/next.')
    vlc_rc.next()
    return redirect("/dashboard", code=302)


@app.route('/action/update_presentation')
def update_advert():
    core_logger.info('Action/update presentation.')
    update_presentation(presentation_id=settings[PRESENTATION_ID])
    return redirect("/dashboard", code=302)


@app.route('/action/update_municipal_radio')
def update_radio():
    core_logger.info('Action/update municipal radio.')
    update_municipal_radio()
    return redirect("/dashboard", code=302)


@app.route('/action/play_municipal_radio')
def play_radio():
    core_logger.info('Action/play municipal radio.')
    files = listdir(MUNICIPAL_RADIO_FOLDER)
    files.sort()
    # TODO directory is empty
    name = files[-1]
    file = join(MUNICIPAL_RADIO_FOLDER, name)
    vlc_rc.add_directory_and_delete_rest(file)
    return redirect("/dashboard", code=302)


@app.route('/action/play_clips')
def play_clips():
    vlc_rc.add_directory_and_delete_rest(CLIPS_FOLDER)
    return redirect("/dashboard", code=302)


@app.route('/action/play_presentation')
def play_advert():
    core_logger.info('Action/play clips.')
    files = listdir(PRESENTATIONS_FOLDER)
    files.sort()
    # TODO directory is empty
    name = files[-1]
    file = join(PRESENTATIONS_FOLDER, name)
    vlc_rc.add_directory_and_delete_rest(file)

    if settings[DELETE_OLD_PRESENTATIONS]:
        for presentation in files:
            if not presentation == name:
                rmtree(join(PRESENTATIONS_FOLDER, presentation), ignore_errors=True)
    return redirect("/dashboard", code=302)


@app.route('/ping')
def ping():
    core_logger.info('Action/ping.')
    return 'ok'


@app.route('/vlc')
def vlc():
    core_logger.info('Redirect to VLC web interface.')
    redirect_url = "http://"+request.headers['Host'].split(':')[0]+':'+str(settings[VLC_WEB_PORT])
    return redirect(redirect_url, code=302)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/action/update_presentation')
def update_presentation_call():
    update_presentation(presentation_id=settings[PRESENTATION_ID],
                        seconds_per_slide=settings[SECONDS_PER_SLIDE],
                        video_codec=settings[PRESENTATION_VIDEO_CODEC])
    return redirect("/dashboard", code=302)


@app.route('/file-move/<file>/<source>/<target>', methods=['GET'])
def move_file(file, source, target):
    source = source.split('-', 1)[1]
    target = target.split('-', 1)[1]
    source_file_path = join(DATA_FOLDER, source, file)
    response = {}
    if not isfile(source_file_path):
        response['status'] = 'error'
        response['reason'] = 'File does not exist.'
        return jsonify(response)
    destination_directory = join(DATA_FOLDER, target)
    if not isdir(destination_directory):
        response['status'] = 'error'
        response['reason'] = 'Target directory does not exist.'
        return jsonify(response)
    destination_file_path = join(destination_directory, file)
    if isfile(destination_file_path):
        response['status'] = 'error'
        response['reason'] = 'Destination file already exists.'
        return jsonify(response)
    try:
        move(source_file_path, destination_file_path)
    except OSError as e:
        response['status'] = 'error'
        response['reason'] = str(e)
        return jsonify(response)
    response['status'] = 'ok'
    response['reason'] = 'Copied.'
    return jsonify(response)


def run_http(host='127.0.0.1', port=5000, debug=False):
    # generate presentation
    if not debug:
        update_presentation(settings[PRESENTATION_ID])
    app.run(host=host, port=port, debug=debug, use_reloader=False)

    return app


if __name__ == '__main__':
    core_logger.info('Starting app.')
    run_http(debug=True)
