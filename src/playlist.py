from collections import OrderedDict
import re


FIRST_TRACK_INDEX = 2
TRACK_BEGINNING = '|  '


class Playlist(OrderedDict):
    playing = None

    def __init__(self, playlist_string):
        super(Playlist, self).__init__()
        lines = playlist_string.split('\n')
        for line in lines[FIRST_TRACK_INDEX:]:
            if line.startswith(TRACK_BEGINNING):
                # print(line)
                # |  *816 - Letecká píseň (orig) (00:02:04) [played 1 time]
                m = re.search(r"(\*?)(\d+) - (.+)(?=\((\d{2}\:\d{2}\:\d{2})\))", line)
                try:
                    track = Track(index=m.group(2), name=m.group(3), duration=m.group(4), is_playing=m.group(1))
                    self[m.group(2)] = track
                    if m.group(1) == '*':
                        self.playing = track
                except AttributeError:
                    continue
            else:
                # do not process media library
                break

    def __str__(self):
        return '\n'.join(str(x) for x in self.values())


class Track:

    def __init__(self, index, name, duration, is_playing=''):
        self.index = index
        self.name = name
        self.duration = duration
        self.is_playing = is_playing

    def __str__(self):
        return '{} - {} ({}){}'.format(self.index, self.name, self.duration, self.is_playing)
