import requests
from datetime import datetime
import os
from pdf2image import convert_from_path
from settings import *


def update_presentation(presentation_id, seconds_per_slide=5, video_codec='libx264'):
    core_logger.info('Updating presentation.')
    time = datetime.now().strftime('%Y%m%d%H%M%S')
    folder = join(PRESENTATIONS_FOLDER, time)
    try:
        os.mkdir(folder)
    except OSError as e:
        core_logger.error('Cannot create presentation directory {}'.format(folder))
        raise e

    filename = 'presentation_{}.pdf'.format(time)
    path = join(folder, filename)

    # TODO URL does not exists
    url = 'https://docs.google.com/presentation/d/{}/export/pdf'.format(presentation_id)
    core_logger.info('Downloading {}.'.format(url))
    r = requests.get(url)

    with open(path, 'wb') as f:
        f.write(r.content)
    core_logger.info('Converting pdf {} to images.'.format(path))
    images = convert_from_path(path)

    for index, image in enumerate(images):
        extrema = image.convert("L").getextrema()
        if extrema[0] == extrema[1]:
            break
        else:
            image_path = join(folder, 's{:03d}.png'.format(index))
            image.save(image_path)

    output_name = 'presentation_{}.mp4'.format(time)
    output_path = join(folder, output_name)
    command = "ffmpeg -framerate 1/{} -i {}/s%03d.png -r 25 -vcodec {} -y {}".format(
        seconds_per_slide,
        folder,
        video_codec,
        output_path)
    core_logger.info('FFMPEG images to video: {}'.format(command))

    os.system(command)

    return output_path


def transcode_video(source, destination, insert_logo=True):
    logo = ''

    if insert_logo:
        logo = ' -i {} -filter_complex "overlay={}:{}"'.format(
            join(DATA_FOLDER, 'logo', settings[LOGO_NAME]),
            settings[LOGO_X],
            settings[LOGO_Y])

    command = 'ffmpeg -i {}{} -c:v {} -preset slow -crf 22 -c:a copy {}'.format(
        source,
        logo,
        settings[VIDEO_OUTPUT_CODEC],
        destination
    )
    core_logger.info('Converting video: {}'.format(command))
    os.system(command)



def audio_to_video(audio, image, destination):
    command = 'ffmpeg -loop 1 -i {} -i {} -c:v libx264 -tune stillimage -c:a aac -b:a 192k ' \
              '-pix_fmt yuv420p -shortest {}'.format(image, audio, destination)
    core_logger.info('Converting audio to video: {}.'.format(command))
    os.system(command)


def update_municipal_radio():
    core_logger.info('Updating municipal radio video.')
    time = datetime.now().strftime('%Y%m%d%H%M%S')
    folder = join(MUNICIPAL_RADIO_FOLDER, time)
    try:
        os.mkdir(folder)
    except OSError as e:
        core_logger.info('Cannot create {} directory.'.format(folder))
        raise e

    filename = 'radio_{}.mp3'.format(time)
    audio_file = join(folder, filename)

    # TODO URL does not exists
    #url = 'http://www.znc.cz/layout/files/1359204445_05%20-%20Balonek.mp3'
    url = 'https://www.mediacollege.com/audio/tone/files/1kHz_44100Hz_16bit_05sec.mp3'

    core_logger.info('Downloading audio file {}.'.format(url))
    r = requests.get(url)

    with open(audio_file, 'wb') as f:
        f.write(r.content)

    output_name = 'radio_{}.mp4'.format(time)
    output_file = join(folder, output_name)
    image_file = join(LOGO_FOLDER, settings[MUNICIPAL_RADIO_IMAGE])

    audio_to_video(audio_file, image_file, output_file)
    core_logger.info('Audio converted - {}.'.format(output_file))
    return output_file


def do_action(url):
    print('{} - Testing {}'.format(datetime.now().time(), url))


if __name__ == '__main__':
    update_municipal_radio()
    # audio_to_video('/home/vaclav/PycharmProjects/vlc_sc/data/logo/sine.mp3',
    #               '/home/vaclav/PycharmProjects/vlc_sc/data/logo/test-pattern.png',
    #               'test.mpg')

