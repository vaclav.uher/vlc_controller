from os import  makedirs
from os.path import dirname, realpath, join, exists
from yaml import safe_load, dump
from logger import core_logger


MUNICIPAL_RADIO = 'municipal_radio'
ADVERTISEMENT = 'advertisement'
PRESENTATIONS = 'presentations'
CLIPS = 'clips'
UNUSED = 'unused'


ROOT_FOLDER = dirname(dirname(realpath(__file__)))
DATA_FOLDER = join(ROOT_FOLDER, 'data')
SETTINGS_FOLDER = join(ROOT_FOLDER, 'settings')
SETTINGS_FILE = join(SETTINGS_FOLDER, 'settings.yaml')
LOGO_FOLDER = join(DATA_FOLDER, 'logo')
MUNICIPAL_RADIO_FOLDER = join(DATA_FOLDER, MUNICIPAL_RADIO)
ADVERTISEMENT_FOLDER = join(DATA_FOLDER, ADVERTISEMENT)
PRESENTATIONS_FOLDER = join(DATA_FOLDER, PRESENTATIONS)
CLIPS_FOLDER = join(DATA_FOLDER, CLIPS)
UNUSED_FOLDER = join(DATA_FOLDER, UNUSED)


RC_HOST = 'rc_host'
RC_PORT = 'rc_port'
HTTP_HOST = 'http_host'
HTTP_PORT = 'http_port'
HTTP_DEBUG = 'http_debug'
VLC_WEB_INTERFACE = 'vlc_web_interface'
VLC_WEB_PORT = 'vlc_web_port'
VLC_PASSWORD = 'vlc_web_password'
RUN_VLC = 'run_vlc'
PRESENTATION_ID = 'presentation_id'
SECONDS_PER_SLIDE = 'seconds_per_slide'
PRESENTATION_VIDEO_CODEC = 'presentation_video_codec'
LOGO_NAME = 'logo_name'
LOGO_X = 'logo_x'
LOGO_Y = 'logo_y'
VIDEO_OUTPUT_CODEC = 'video_output_codec'
DELETE_OLD_PRESENTATIONS = 'delete_old_presentations'
MUNICIPAL_RADIO_IMAGE = 'municipal_radio_image'

# day shortcut constants
MON = 'monday'
TUE = 'tuesday'
WED = 'wednesday'
THU = 'thursday'
FRI = 'friday'
SAT = 'saturday'
SUN = 'sunday'


ALLOWED_VIDEO = {'avi', 'mp4', 'mpg', 'mpeg', 'mov', 'mkv', 'wmv'}
ALLOWED_AUDIO = {'mp3', 'ogg', 'wav'}
ALLOWED_IMAGE = {'jpg', 'jpeg', 'png', 'bmp', 'tif'}
ALLOWED_DOCUMENTS = {'pdf'}
ALLOWED_EXTENSIONS = ALLOWED_AUDIO | ALLOWED_VIDEO

# Create directories
if not exists(DATA_FOLDER):
    core_logger.debug('Creating folder {}.'.format(DATA_FOLDER))
    makedirs(DATA_FOLDER)
if not exists(SETTINGS_FOLDER):
    core_logger.debug('Creating folder {}.'.format(SETTINGS_FOLDER))
    makedirs(SETTINGS_FOLDER)
if not exists(LOGO_FOLDER):
    core_logger.debug('Creating folder {}.'.format(LOGO_FOLDER))
    makedirs(LOGO_FOLDER)
if not exists(MUNICIPAL_RADIO_FOLDER):
    core_logger.debug('Creating folder {}.'.format(MUNICIPAL_RADIO_FOLDER))
    makedirs(MUNICIPAL_RADIO_FOLDER)
if not exists(ADVERTISEMENT_FOLDER):
    core_logger.debug('Creating folder {}.'.format(ADVERTISEMENT_FOLDER))
    makedirs(ADVERTISEMENT_FOLDER)
if not exists(PRESENTATIONS_FOLDER):
    core_logger.debug('Creating folder {}.'.format(PRESENTATIONS_FOLDER))
    makedirs(PRESENTATIONS_FOLDER)
if not exists(CLIPS_FOLDER):
    core_logger.debug('Creating folder {}.'.format(CLIPS_FOLDER))
    makedirs(CLIPS_FOLDER)
if not exists(UNUSED_FOLDER):
    core_logger.debug('Creating folder {}.'.format(UNUSED_FOLDER))
    makedirs(UNUSED_FOLDER)


def load_default_settings():

    defaults = {
        RC_HOST: '127.0.0.1',
        RC_PORT: 4212,
        HTTP_HOST: '127.0.0.1',
        HTTP_PORT: 5000,
        HTTP_DEBUG: True,
        VLC_WEB_INTERFACE: True,
        VLC_WEB_PORT: 8080,
        VLC_PASSWORD: 'heslo',
        RUN_VLC: True,
        PRESENTATION_ID: '1TtM-DAeOFrLo1MpwY4vpNHUkCgaVJkHkUAjzwZwxljc',
        SECONDS_PER_SLIDE: 5,
        PRESENTATION_VIDEO_CODEC: 'mpeg4',
        LOGO_NAME: 'logo.png',
        LOGO_X: 20,
        LOGO_Y: 20,
        VIDEO_OUTPUT_CODEC: 'libx264',
        DELETE_OLD_PRESENTATIONS: True,
        MUNICIPAL_RADIO_IMAGE: 'test-pattern.png'

    }
    core_logger.info('Loading default settings: {}'.format(defaults))
    return defaults


def load_settings(filename=SETTINGS_FILE):
    yaml_settings = None
    try:
        with open(filename) as yaml_file:
            core_logger.info('Opening settings file {}.'.format(filename))
            yaml_settings = safe_load(yaml_file)
    except FileNotFoundError:
        # file not found, generate one with default settings
        core_logger.warn('Settings file {} not found.'.format(filename))
        yaml_settings = load_default_settings()
        with open(filename, 'w') as yaml_file:
            core_logger.info('Saving default settings to {}.'.format(filename))
            dump(yaml_settings, yaml_file)

    except Exception as e:
        # only catch bad formatting, do not overwrite with default values
        core_logger.warn('Problem with loading YAML settings file{}.'.format(filename))
        load_default_settings()

    return yaml_settings


settings = load_settings()


if __name__ == '__main__':
    print(load_settings())
