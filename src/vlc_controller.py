import time
import subprocess
import signal
import os
from settings import *


class VlcController:

    process = None

    def __init__(self):
        self.settings = ['vlc']
        self.settings.extend([
                              # '-vvv',
                              # '--file-logging', '--logfile={}'.format(os.path.abspath('../vlc.log')),
                              '-I', 'rc', '--rc-host', '{}:{}'.format(settings[RC_HOST], settings[RC_PORT]),
                              '--loop',
                              '--sout-keep'
                            ])
        if settings[VLC_WEB_INTERFACE]:
            self.settings.extend(['--extraintf', 'http',
                                  '--http-port', str(settings[VLC_WEB_PORT]),
                                  '--http-password', settings[VLC_PASSWORD]
                                  ])
        self.settings.extend([os.path.abspath('../settings/test.mp4')])
        self.settings.extend(['--sout', '#rtp{mux=ts,dst=127.0.0.1,sdp=sap,name="JMorava.net TV Stream"}'])

    def start(self):
        if self.process:
            self.stop()
        try:
            core_logger.info(subprocess.check_output(['killall', 'vlc']))
        except Exception:
            core_logger.info('VLC is not running.')
            # VLC is not running

        core_logger.info('Starting VLC: {}'.format(' '.join(self.settings)))
        self.process = subprocess.Popen(self.settings)

        time.sleep(2)

    def stop(self):
        if self.process:
            self.process.terminate()
            os.kill(self.process.pid, signal.SIGTERM)
            self.process = None


if __name__ == '__main__':
    vc = VlcController()
    vc.start()
    time.sleep(15)
    vc.stop()
