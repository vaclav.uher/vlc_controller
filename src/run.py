import os
from vlc_connector import VLCClient

vlc = VLCClient('127.0.0.1', port=4212, password='heslo')
vlc.connect()
file = '../data/07 - Florián a Kilián.mp3'

abs_path = os.path.abspath(file)

print(vlc.playlist())
vlc.add_and_delete_rest([abs_path, abs_path])
print(vlc.playlist())
# print(vlc.delete(26))
# print(vlc.longhelp())
# vlc.add(abs_path)
#vlc.clear()